OAuth3 Issuer Implementation
============================

| [oauth3.js](https://git.oauth3.org/OAuth3/oauth3.js)
| [issuer.html](https://git.oauth3.org/OAuth3/issuer.html)
| [issuer.rest.walnut.js](https://git.oauth3.org/OAuth3/issuer.rest.walnut.js)
| *issuer.srv*
| Sponsored by [ppl](https://ppl.family)

This is a reference implementation of an OAuth3 identity issuer.

Instructions
------------

Boot up a Digital Ocean VPS or a Docker or an Ubuntu or whatever it is that you do and then do this:

Install the Goldilocks Net Server (for automatic HTTPS via ACME):

```bash
curl https://git.coolaj86.com/coolaj86/goldilocks.js/raw/v1.1/installer/get.sh | bash
```

Use the sample goldilocks config file and replace "example.com" with whatever domain you want to use:

`/etc/goldilocks/goldilocks.yml`:
```yml
socks5:
  enabled: false
mdns:
  disabled: true
  port: 5353
  broadcast: 224.0.0.251
  ttl: 300
domains:
  - names:
      - www.example.com
      - example.com
      - api.example.com
      - assets.example.com
      - webhooks.example.com
      - ssh.example.com
      - vpn.example.com
    modules:
      http:
        - type: proxy
          port: 3000
      tls:
        - type: acme
          email: coolaj86@gmail.com
      tcp: []
udp:
  bind: []
tcp:
  modules:
    - domains:
        - ssh.example.com
      port: 22
      type: proxy
    - domains:
        - vpn.example.com
      port: 1194
      type: proxy
  bind:
    - 80
    - 443
http:
  modules: []
tls:
  modules: []
ddns:
  modules: []
```

Go update your DNS records for those domains to point to this server. However you do that...

Install the WALNUT application server:

```bash
curl https://git.coolaj86.com/coolaj86/walnut.js/raw/v1.2/installer/get.sh | bash
```

Then update the walnut grants to allow your site to use the specified APIs and packages:

```bash
echo "issuer@oauth3.org" >> /opt/walnut/etc/client-api-grants/example.com
echo "issuer@oauth3.org" >> /opt/walnut/var/sites/example.com
```

Get a mailgun account, verify your domain, and add your API keys:

```bash
# example.com will work for specific hard-coded subdomains (api., assets., webhooks.)
mkdir -p /opt/walnut/var/example.com/
```

`/opt/walnut/var/example.com/config.json`:
```js
{ "mailgun.org": {
    "apiKey": "key-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
   , "auth": {
      "user": "mailer@example.com"
    , "pass": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    , "api_key": "key-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    , "domain": "example.com"
    }
  }
}
```

Change the email address used to send in `/opt/walnut/packages/rest/issuer@oauth3.org/accounts.js` (make it match your mailgun.org account).
